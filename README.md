# deepstream_app

A bare deepstream-app using CMake

## Dependencies

```bash
sudo apt-get install libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev \
   libgstrtspserver-1.0-dev libx11-dev libjson-glib-dev
```
